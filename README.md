Phalcony Full Module Skeleton
==

This is the skeleton used by Phalcony Devtools to create new modules on Phalcony projects.

Phalcony relies entirely on annotations, so the code and it's settings walk always together.

API (Controllers and CLI)
--
As stated before, you will make a big use of annotations on the module.  Modules will only work if you use annotations 
for the API. Controllers and Console commands should be create for the API definition, and must be annotated to work. 
To setup a route, you need to annotate it. Also, to have a command avaliable, you must annotate it.

Controllers and Console commands receive data wrapped as a Model/Dto/Request (Request Data Transfer Objects) and passes
it to the Business Layer to be processed. After processing the data, Business layer is also supposed to return a 
Model/Dto/Response wrapper.

Business, Validations and Exceptions
--
All Business rules and validations are supposed to be encapsulated inside the Business package. They also are supposed 
to have their own specific Exceptions. All other layers throw specific layer exceptions provided by Phalcony, so, you 
don't need to worry about them.

Events and Jobs
--
A Module allow you to easily create Event Dispatchers and Listeners. All you need to do is to add them to the 
provided namespace and Phalcony will automatically register them. All you have to do is call them by the event name
dispatcher you defined on your code, and the EventManager will handle it automatically. Usually an event dispatcher will
register a job for execution but it could also trigger another business on the same, or on a different, module.

Also Schedulable and Queueable Jobs can be created easily by just adding them to the provided namespace and setting 
their running times. Queueable Jobs can are registered for execution by Event Listeners.

Model
--
The Model namespace handles the Database Models and the Dtos used for Request and Response. That way you can easily
mantain your data structures and API contracts. Although you could do whatever you want with the Model/Db layer, 
we strongly advice you to get familiar with the Repository Design Pattern and use the Repository namespace to keep 
your data repository manipulations on one place.

Model/Dto wrappers are important because they will always be processed for serialization and deserialization of 
JSON data. The Phalcony serialization layer is very powerfull and can handle any kind of data structure to 
serialization, even if circular references are found on these data structures, but writing a Model/Dto will help
you to have better control of what data is flowing from your backend to the frontend and vice-versa

Services
--
The Service Layer should be used for 3 pourposes:

* Create a Service Client to consume external services
* Create a Service Provider to be used by the Business Layer
* Create Service Utilities, any kind of tool for transformation, broad data handling, etc.

Resources
--
The Phalcony Module skeleton provides also a Resources space. This space is provided mainly to handle the translations
and the database migrations files, require to keep the Module running, but only as external resources.

Translations are supposed to be CSV files named with the language it contains (en_US.cs, for example) and with a message
code plus the message text. This format has been chosen so the messages can be edited using a simple spreadsheet tool 
like Excel, Libreoffice Calc, etc.

Tests
--
Finally, you have a space to write your tests. Tests are encapsulated on the Module instead of the Application because 
that way you can fork your own Modules and have different tests results for the same API but with different clients 
(just an example).